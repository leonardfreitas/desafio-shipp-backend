
# Desafio - Leonardo Freitas

##### Passos

Testado usando os seguintes SO: 
Linux Ubuntu 19.10, caso teste em windows, por favor verifique se o MSBuild.exe está disponivel

1 - Preparar ambiente

- Instalar dependências: npm install ou yarn
- Migração de banco de dados: npx sequelize db:migrate
- Subir os servidor: npm start (subirá na porta 8000)


2 - Autenticação (exemplos)
- Criar usuário: POST /V1/users =>
```json
{
	"username":"leofreitas",
	"email":"leo@shipp.com", 
	"password": "leo123", 
	"name": "Leonardo Freitas"
}
```
- Login: POST /V1/auth =>
```json
{
	"usernameOrEmail":"leofreitas",
	"password": "leo123"
}
```

3 - Popular banco e buscar stores


**Importante: Ambas requisições precisam do header Authorization no formato Bearer :token, o token é retornado pela rota de login (caso a autenticação tenha sucesso)**



- Popular o banco a partir do arquivo .csv: POST /V1/import-data 

- Listagem de lojas: GET /V1/stores/?lat=42.754424&lon=-73.700539

### Leonardo Freitas de Souza

------------

Endereço para acessar este CV: http://lattes.cnpq.br/2545613146490844
