const csv = require('csvtojson')
const path = require('path')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
    const csvFilePath = path.join(__dirname, '..', 'stores.csv')
    console.log(csvFilePath)

    const dataSet = await csv().fromFile(csvFilePath)
    
    var storeBulk = []

    for (var i in dataSet) {

      var splitLocation = dataSet[i]['Location'].split(',')
      
      const splitLong = splitLocation[0].split(':')
      if (splitLong[1]) {
        const long = splitLong[1].trim()
        if (long !== 'False') {
          dataSet[i]['longitude'] = parseFloat(
            long.substring(1, long.length - 1)
          )
        }        
      }

      const splitLat = splitLocation[splitLocation.length -1].split(':')
      if (splitLat[1]) {
        const lat = splitLat[1].trim()
        if (lat !== 'False') {
          dataSet[i]['latitude'] = parseFloat(
            lat.substring(1, lat.length - 1)
          )
        }        
      }

      storeBulk.push({
        entityName: dataSet[i]['Entity Name'],
        dbaName: dataSet[i]['DBA Name'],
        establishmentType: dataSet[i]['Establishment Type'],
        licenseNumber: dataSet[i]['License Number'],
        operationType: dataSet[i]['Operation Type'],
        squareFootage: dataSet[i]['Square Footage'],
        street: dataSet[i]['Street Name'],
        number: dataSet[i]['Street Number'],
        city: dataSet[i]['City'],
        state: dataSet[i]['State'],
        zipCode: dataSet[i]['Zip Code'],
        country: dataSet[i]['County'],
        longitude: dataSet[i]['longitude'],
        latitude: dataSet[i]['latitude']
      })

    }

    return queryInterface.bulkInsert('stores', storeBulk, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
