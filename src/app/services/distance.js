const Distance = require('geo-distance')

const DISTANCE_DEFAULT = 6.4

function getLocaleDistance (points) {
  const { userLocation, location } = points
  const p1ToP2 = Distance.between(userLocation, location).human_readable()
  const distance = parseFloat(p1ToP2.distance)
  if (distance <= DISTANCE_DEFAULT) return true
  return false
}

module.exports = getLocaleDistance
