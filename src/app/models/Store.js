const Sequelize = require('sequelize')

class Store extends Sequelize.Model {
  static init (sequelize) {
    super.init(
      {
        entityName: Sequelize.STRING,
        dbaName: Sequelize.STRING,
        establishmentType: Sequelize.STRING,
        licenseNumber: Sequelize.STRING,
        operationType: Sequelize.STRING,
        squareFootage: Sequelize.STRING,
        street: Sequelize.STRING,
        number: Sequelize.STRING,
        city: Sequelize.STRING,
        state: Sequelize.STRING,
        zipCode: Sequelize.STRING,
        country: Sequelize.STRING,
        longitude: Sequelize.FLOAT,
        latitude: Sequelize.FLOAT
      },
      {
        sequelize
      }
    )

    return this
  }
}

module.exports = Store
