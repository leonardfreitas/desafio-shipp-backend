const Sequelize = require('sequelize')

class Address extends Sequelize.Model {
  static init (sequelize) {
    super.init(
      {
        street: Sequelize.STRING,
        number: Sequelize.STRING,
        city: Sequelize.STRING,
        state: Sequelize.STRING,
        zipCode: Sequelize.STRING,
        country: Sequelize.STRING,
        longitude: Sequelize.FLOAT,
        latitude: Sequelize.FLOAT
      },
      {
        sequelize
      }
    )

    return this
  }
}

module.exports = Address
