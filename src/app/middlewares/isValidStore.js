const isValidStore = (req, res, next) => {
  if (!(req.query && req.query.lat && req.query.lon)) {
    return res.status(400).json({
      error: 'Os parâmetros lat e lon são necessários'
    })
  }
  return next()
}

module.exports = isValidStore
