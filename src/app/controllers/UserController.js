const Yup = require('yup')
const Sequelize = require('sequelize')
const User = require('../models/User')

const Op = Sequelize.Op

class UserController {
  /*
    Create user
    post /users
  */
  async store (req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      email: Yup.string().required(),
      username: Yup.string().required(),
      password: Yup.string().required()
    })

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Falha na validação' })
    }

    const userExists = await User.findOne({
      where: {
        [Op.or]: [{ username: req.body.username }, { email: req.body.email }]
      }
    })

    if (userExists) {
      return res.status(400).json({ error: 'Usuário já existente' })
    }

    const { id, name, username, email } = await User.create(req.body)

    return res.json({ id, name, username, email })
  }
}

module.exports = new UserController()
