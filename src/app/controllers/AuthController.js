const Yup = require('yup')
const jwt = require('jsonwebtoken')
const Sequelize = require('sequelize')

const User = require('../models/User')
const authConfig = require('../../config/auth')

const Op = Sequelize.Op

class AuthController {
  /*
    login user
    post /auth
  */
  async store (req, res) {
    const schema = Yup.object().shape({
      usernameOrEmail: Yup.string().required(),
      password: Yup.string()
    })

    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Falha na validação' })
    }

    const { usernameOrEmail, password } = req.body

    const user = await User.findOne({
      where: {
        [Op.or]: [{ username: usernameOrEmail }, { email: usernameOrEmail }]
      }
    })

    if (!user) {
      return res.status(401).json({ error: 'Usuário não encontrado' })
    }

    if (user.status === false) {
      return res
        .status(401)
        .json({ error: 'Usuário inativo, contate a gestão' })
    }

    if (!(await user.checkPassword(password))) {
      return res
        .status(401)
        .json({ error: 'Senha não corresponde com esse usuário' })
    }

    const { id } = user

    return res.json({
      token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn
      })
    })
  }

  /**
   * verify token
   */
  async validToken (req, res) {
    try {
      jwt.verify(req.body.token, authConfig.secret, (err, authorizedData) => {
        if (err) return res.json({ valid: false })
        return res.json({ valid: true })
      })
    } catch (error) {
      console.log(error)
      return res
        .status(500)
        .json({ error: 'Erro de servidor. Contate o suporte' })
    }
  }
}

module.exports = new AuthController()
