const Sequelize = require('sequelize')
const csv = require('csvtojson')
const path = require('path')
const Distance = require('geo-distance')

const getLocaleDistance = require('../services/distance')
const Store = require('../models/Store')

const Op = Sequelize.Op

class StoreController {
  /**
   * import data
   */
  async importData (req, res) {
    const csvFilePath = path.join(__dirname, 'stores.csv')

    const dataSet = await csv().fromFile(csvFilePath)

    var storeBulk = []

    for (var i in dataSet) {
      var splitLocation = dataSet[i].Location.split(',')

      const splitLong = splitLocation[0].split(':')
      if (splitLong[1]) {
        const long = splitLong[1].trim()
        if (long !== 'False') {
          dataSet[i].longitude = parseFloat(long.substring(1, long.length - 1))
        }
      }

      const splitLat = splitLocation[splitLocation.length - 1].split(':')
      if (splitLat[1]) {
        const lat = splitLat[1].trim()
        if (lat !== 'False') {
          dataSet[i].latitude = parseFloat(lat.substring(1, lat.length - 1))
        }
      }

      storeBulk.push({
        entityName: dataSet[i]['Entity Name'],
        dbaName: dataSet[i]['DBA Name'],
        establishmentType: dataSet[i]['Establishment Type'],
        licenseNumber: dataSet[i]['License Number'],
        operationType: dataSet[i]['Operation Type'],
        squareFootage: dataSet[i]['Square Footage'],
        street: dataSet[i]['Street Name'],
        number: dataSet[i]['Street Number'],
        city: dataSet[i].City,
        state: dataSet[i].State,
        zipCode: dataSet[i]['Zip Code'],
        country: dataSet[i].County,
        longitude: dataSet[i].longitude,
        latitude: dataSet[i].latitude
      })
    }

    Store.bulkCreate(storeBulk)
      .then(() => {
        return res.json({ msg: 'imported' })
      })
      .catch(error => {
        return res.status(500).json({ error: 'Erro ao importar dados' })
      })
  }

  /**
   * list stores
   */
  async list (req, res) {
    const R = 6.5

    // Exemples
    // lat: 42.754424
    // lon: -73.700539
    const userLocation = {
      lat: req.query.lat,
      lon: req.query.lon
    }

    const stores = await Store.findAll()

    function setDistance (store) {
      const location = {
        lat: store.latitude,
        lon: store.longitude
      }
      return getLocaleDistance({ userLocation, location })
    }

    const storesFiltered = stores.filter(setDistance)

    return res.json({ stores: storesFiltered })
  }
}

module.exports = new StoreController()
