module.exports = {
  dialect: 'sqlite',
  storage: './store.sqlite3',
  define: {
    timestamps: true
  }
}
