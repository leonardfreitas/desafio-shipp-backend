const express = require('express')

const authMiddleware = require('./app/middlewares/auth')
const isValidStore = require('./app/middlewares/isValidStore')

const AuthController = require('./app/controllers/AuthController')
const UserController = require('./app/controllers/UserController')
const StoreController = require('./app/controllers/StoreController')

const routes = new express.Router()

routes.post('/auth', AuthController.store)
routes.post('/verify-token', AuthController.validToken)

// create user for test
routes.post('/users', UserController.store)

// middleware auth
routes.use(authMiddleware)
routes.post('/import-data', StoreController.importData)
routes.get('/stores', isValidStore, StoreController.list)

routes.use(authMiddleware)

module.exports = routes
