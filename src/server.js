const express = require('express')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const bodyParser = require('body-parser')
const routes = require('./routes')
require('./database')

class App {
  constructor () {
    this.server = express()

    this.middlewares()
    this.routes()
  }

  middlewares () {
    this.server.use(cors())
    this.server.use(express.json())
    this.server.use(bodyParser.urlencoded({ extended: true }))
    this.server.use(
      fileUpload({
        createParentPath: true
      })
    )
  }

  routes () {
    this.server.use('/V1', routes)
  }
}

module.exports = new App().server
