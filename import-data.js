const Store = require('./src/app/models/Store')
const Address = require('./src/app/models/Address')

const csv = require('csvtojson')

const csvFilePath = 'stores.csv'

Address.create({
  street: 'Rua Senador Pompeu',
  number: '100',
  city: 'Crato',
  state: 'CE',
  zipCode: '87984646',
  country: 'Brasil',
  longitude: -78.254664,
  latitude: 54.554578
})
.then(address => {
  console.log(address);
})

csv()
.fromFile(csvFilePath)
.then((dataSet) => {
    dataSet.map((store) => {
      splitLocation = store['Location'].split(',')
      
      const splitLong = splitLocation[0].split(':')
      if (splitLong[1]) {
        const long = splitLong[1].trim()
        if (long !== 'False') {
          store['longitude'] = parseFloat(
            long.substring(1, long.length - 1)
          )
        }        
      }

      const splitLat = splitLocation[splitLocation.length -1].split(':')
      if (splitLat[1]) {
        const lat = splitLat[1].trim()
        if (lat !== 'False') {
          store['latitude'] = parseFloat(
            lat.substring(1, lat.length - 1)
          )
        }        
      }

      delete store['Address Line 2']
      delete store['Address Line 3']
      delete store['Location']

      Address.create({
        street: store['Street Name'],
        number: store['Street Number'],
        city: store['City'],
        state: store['State'],
        zipCode: store['Zip Code'],
        country: store['County'],
        longitude: store['longitude'],
        latitude: store['latitude']
      })
      .then(address => {
        console.log(address);
      })

      // console.log(store)
    })
})