// const csvToJson = require('convert-csv-to-json')
const fs = require('fs')
const path = require('path')
const csvjson = require('csvjson')

var data = fs.readFileSync(
  path.join(__dirname, 'stores.csv'), 
  { encoding : 'utf8'}
)

// const fileInput = 'stores.csv'

// // csvToJson.generateJsonFileFromCsv(fileInput, fileOutput)

// csvToJson.fieldDelimiter(',').getJsonFromCsv(fileInput)
// csvToJson.formatValueByType().getJsonFromCsv(fileInput);

// let json = csvToJson.getJsonFromCsv("stores.csv");
// json.map((store, index) => {
//   delete store.AddressLine2
//   delete store.AddressLine3
//   console.log(store)
//   // console.log(item['Location'])
// })

var options = {
  delimiter : ',',
  quote: '"',
  headers: 'County,License Number,Operation Type,Establishment Type,Entity Name,DBA Name,Street Number,Street Name,Address Line 2,Address Line 3,City,State,Zip Code,Square Footage,Location.longitude,Location.latitude'
};

const json = csvjson.toObject(data, options);
console.log(json)